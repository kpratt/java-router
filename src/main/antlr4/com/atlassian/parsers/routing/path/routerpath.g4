grammar routerpath;

options
{
  // antlr will generate java lexer and parser
  language = Java;
}

@lexer::members {
}

@parser::members{
}

// ***************** lexer rules:
//the grammar must contain at least one lexer rule

PARAM  : ':' ;
REST   : '*' ;
SLASH  : '/' ;
NUMBER : DIGIT+  ;
WORD   : ALPHA+  ;

fragment DIGIT   : [0-9] ;
fragment ALPHA   : [a-zA-Z] ;

// ***************** parser rules:
//our grammar accepts only salutation followed by an end symbol
parseRoot  : SLASH pathElems SLASH?
           | SLASH
           ;

pathElems  : PARAM WORD (SLASH pathElems)*
           | REST WORD
           | WORD (SLASH pathElems)*
           ;
