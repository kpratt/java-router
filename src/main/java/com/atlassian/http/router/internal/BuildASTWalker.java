package com.atlassian.http.router.internal;

import com.atlassian.parsers.routing.path.routerpathLexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.List;

public class BuildASTWalker implements ParseTreeListener {
    List<PathMatcher.SegmentMatcher> matchers;
    PathMatcher.TailMatcher tail;

    Mode mode;

    enum Mode {
        Static,
        Param,
        Rest,
        Done
    }

    public BuildASTWalker() {
        matchers = new ArrayList<>();
        tail = null;
        mode = Mode.Static;
    }

    @Override
    public void visitTerminal(TerminalNode node) {
        int symType = node.getSymbol().getType();
        if(mode == Mode.Done) {
            return;
        } else if(symType == routerpathLexer.PARAM) {
            mode = Mode.Param;
        } else if(symType == routerpathLexer.REST) {
            mode = Mode.Rest;
        } else if(symType == routerpathLexer.WORD) {
            if(mode == Mode.Static){
                matchers.add(new PathMatcher.Static(node.getText()));
            } else if(mode == Mode.Param){
                matchers.add(new PathMatcher.Parameter(node.getText()));
            } else if(mode == Mode.Rest){
                tail = new PathMatcher.TailMatcher(node.getText());
                mode = Mode.Done;
                return;
            }
            mode = Mode.Static;
        }
    }

    @Override
    public void visitErrorNode(ErrorNode node) {

    }

    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
    }

    @Override
    public void exitEveryRule(ParserRuleContext ctx) {

    }
}
