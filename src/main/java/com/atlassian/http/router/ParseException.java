package com.atlassian.http.router;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;

import java.util.BitSet;

public class ParseException extends RuntimeException {
    public static ANTLRErrorListener LISTENER = new BaseErrorListener() {
        @Override
        public void syntaxError(
                Recognizer<?, ?> recognizer,
                Object offendingSymbol,
                int line,
                int charPositionInLine,
                String msg,
                RecognitionException e
        ) {
            throw new ParseException();
        }

        @Override
        public void reportAmbiguity(
                Parser recognizer,
                DFA dfa,
                int startIndex,
                int stopIndex,
                boolean exact,
                BitSet ambigAlts,
                ATNConfigSet configs
        ) {
            throw new ParseException();
        }

        @Override
        public void reportAttemptingFullContext(
                Parser recognizer,
                DFA dfa, int startIndex,
                int stopIndex,
                BitSet conflictingAlts,
                ATNConfigSet configs
        ) {
            throw new ParseException();
        }

        @Override
        public void reportContextSensitivity(
                Parser recognizer, DFA dfa,
                int startIndex,
                int stopIndex,
                int prediction,
                ATNConfigSet configs
        ) {
            throw new ParseException();
        }
    };
}
