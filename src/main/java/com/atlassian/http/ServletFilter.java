package com.atlassian.http;

public interface ServletFilter {
    Servlet around(Servlet inner);
}
