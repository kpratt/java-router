package com.atlassian.http.router;

import com.atlassian.parsers.routing.path.routerpathLexer;
import com.atlassian.parsers.routing.path.routerpathParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ConsoleErrorListener;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.tool.GrammarParserInterpreter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Helpers {
    public static List<Token> lex(String str) throws IOException {
        List<Token> list = new ArrayList<>();
        InputStream stream = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
        CharStream input = new ANTLRInputStream(stream);
        routerpathLexer lexer = new routerpathLexer(input);


        Token flag = null;
        do {
            list.add(flag = lexer.nextToken());
        } while (flag.getType() != Token.EOF);
        return list;
    }

    public static Tree parse(String str) throws IOException {
        InputStream stream = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));

        CharStream input = new ANTLRInputStream(stream);
        routerpathLexer lexer = new routerpathLexer(input);
        lexer.removeErrorListener(ConsoleErrorListener.INSTANCE);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        routerpathParser parser = new routerpathParser(tokens);
        parser.setErrorHandler(new GrammarParserInterpreter.BailButConsumeErrorStrategy());
        Listener l = new Listener();
        parser.addErrorListener(ParseException.LISTENER);
        parser.addParseListener(l);
        ParseTree tree = parser.parseRoot();

        if(tokens.LT(1).getType() != Token.EOF){
            throw new ParseException();
        }
        return l.stack.pop();
    }
    static Tree tree(final String nom, final Tree... xs) {
        Tree tree = new Tree();
        tree.name = nom;
        tree.children = Arrays.<Tree>asList(xs);
        return tree;
    }

    static class Listener implements ParseTreeListener {
        Stack<Tree> stack = new Stack<>();

        public Listener() {
            Tree tree = new Tree();
            tree.name = "<root>";
            stack.push(tree);
        }

        @Override
        public void visitTerminal(TerminalNode node) {
            stack.peek().children.add(tree(node.getSymbol().getText()));
        }

        @Override
        public void visitErrorNode(ErrorNode node) {
            throw new RuntimeException();
        }

        @Override
        public void enterEveryRule(ParserRuleContext ctx) {
            if (ctx.invokingState >= 0) {
                String name = routerpathParser.ruleNames[ctx.getRuleIndex()];
                Tree tree = new Tree();
                tree.name = name;
                stack.push(tree);
            }

        }

        @Override
        public void exitEveryRule(ParserRuleContext ctx) {
            if (ctx.invokingState >= 0) {
                Tree t = stack.pop();
                stack.peek().children.add(t);
            }
        }
    }

    static class Tree {
        String name;
        List<Tree> children;

        public Tree() {
            name = "";
            children = new ArrayList<>();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Tree tree = (Tree) o;

            if (name != null ? !name.equals(tree.name) : tree.name != null) return false;
            return children != null ? children.equals(tree.children) : tree.children == null;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (children != null ? children.hashCode() : 0);
            return result;
        }

        public String toString() {
            StringBuilder buf = new StringBuilder();
            toString(buf);
            return buf.toString();
        }

        public void toString(StringBuilder buf) {
            buf.append("(");
            buf.append("\"").append(name).append("\"");
            for (Tree child : children) {
                buf.append(", ");
                child.toString(buf);
            }
            buf.append(")");
        }
    }
}
