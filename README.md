Java Router
==============

Golang-esque routing for java code, because XML is bad mojo. 

Spring provides declarative mechanisms for registering servlets and filters. This declarative style trades the readability of how the system is wired for ease of writing new servlets. Readability however, is critical to maintenance of large applications. These are the properties we wish to preserve:

1) Filters should be code navigable from the location the servlet is registered.
2) The order the filters will execute should be obvious without deep knowledge of the the framework.
3) Paths and servlets should be paired in one place s.t. it is trivial to find the servlet given the path and vis versa.
4) Parameters in both the path and query string are as type safe as possible.  
5) When the debugger is stopped in the servlet (or a filter), all filters are visible on the stack.
6) There is a single point of entry for requests, similar to a main function from which the most open ended debugging sessions can begin.

NOTE that:
* 6 is dependent on the library consumer to register only one RootServlet with spring
* 3 is not yet fully supported, there is a plan to add `router.getURL(<servlet refrence>)` for use in links and redirects
* 4 is not implemented, there is plan to add to the path language "/resource/:id:int" and "/resource/:id{<regex>}"

Usage
======

How to interface with Spring-boot:

=======
```java
public class RootServlet extends HttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(Servlet.class);

    private final ServletFilterChain commonFilters;
    private final Router router;

    public Servlet(
            AccessLogger accessLogger, // log all access attempts
            Auth authenticator,        // logged in users only
            UIProvider uiProvider      // if they don't want json serve the ui
    ) {
    
        // NOTE: SFCs are immutable so this chaining
        //  is technically constant time O(6), but think O(N^2)
        commonFilters = ServletFilterChain.
                of(accessLogger).
                around(authenticator).
                around(uiProvider);


        router = new Router().
                // route to the static UI files
                register(Router.Method.GET, "/", commonFilters.build(UIProvider::render)).
                register(Router.Method.GET, "/batch.js", accessLogger.around(UIProvider::batch)).
                // route with a simple param
                register(Router.Method.GET, "/resource/:id", commonFilters.build(ResourceSerlets::getById)).
                // route with variable length
                register(Router.Method.GET, "/static/*paths", commonFilters.build(Serve::imagesAndScripts));
    }


    private void doRoute(
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        try {
            router.handle(request, response);
        } catch (IOException e) {
            LOGGER.error("Uncaught error {}", e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doRoute(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        doRoute(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) {
        doRoute(request, response);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) {
        doRoute(request, response);
    }
}
```

Installation
============

```xml
<dependency>
    <groupId>com.atlassian</groupId>
    <artifactId>java-router</artifactId>
    <version>0.1</version>
</dependency>
```

Tests
=====

```bash
mvn test
```

Contributors
============

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

License
========

Copyright (c) 2017 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.